import React, { lazy, Suspense } from 'react';
import { Route, Switch } from 'react-router-dom';
import urls from './config/urls';
import { default as classroomUrls } from './modules/classroom/config/urls';
import Home from './pages/home/Home';
import ProtectedRoute from './shared/protectedRoute/ProtectedRoute';
import PublicRoute from './shared/publicRoute/PublicRoute';

const Chat = lazy(() => import('./pages/chat/Chat'));
const Login = lazy(() => import('./pages/login/Login'));
const NotFound = lazy(() => import('./pages/notFound/NotFound'));
const Classroom = lazy(() => import('./modules/classroom/ClassroomModule'));

function App() {
  return (
    <div className="App">
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <ProtectedRoute
            redirectUrl={urls.Login}
            exact
            path={urls.Home}
            component={Home}
          />
          <ProtectedRoute
            redirectUrl={urls.Login}
            path={urls.Chat}
            component={Chat}
          />
          <PublicRoute
            redirectUrl={urls.Chat}
            path={urls.Login}
            component={Login}
          />
          <Route path={classroomUrls.Classroom} component={Classroom} />
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </div>
  );
}

export default App;
