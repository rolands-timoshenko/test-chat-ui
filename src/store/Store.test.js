import { doLoginAsync, removeUserProfile } from './user/action';
import { configureStore } from './Store';

describe('Root store', () => {
  let store;
  let dispatch;
  beforeEach(async () => {
    store = configureStore();
    dispatch = store.dispatch;
  });

  it('should set user profile', async () => {
    const asyncAction = doLoginAsync('username', 'password', () => {});
    await asyncAction(dispatch);
    const state = store.getState();
    expect(JSON.stringify(state.user.profile)).toBe(
      JSON.stringify({ username: 'username' })
    );
  });

  it('should remove user profile', async () => {
    dispatch(removeUserProfile());
    const state = store.getState();
    expect(JSON.stringify(state.user.profile)).toBe(JSON.stringify(null));
  });
});
