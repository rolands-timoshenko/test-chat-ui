import React from 'react';
import { ReactReduxContext } from 'react-redux';

const withRootStoreContext = Component => (props) =>  
<ReactReduxContext.Consumer>
    {store => <Component {...props} {...store} />}
</ReactReduxContext.Consumer>


export default withRootStoreContext;