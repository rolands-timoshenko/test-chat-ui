import React from 'react';
import withRootStoreContext from './withRootStoreContext';

const withStore = (alias, reducer) => Component =>
  withRootStoreContext(
    class extends React.Component {
      constructor(props) {
        super(props);
        this.props.store.reducerManager.add(alias, reducer);
      }

      componentWillUnmount() {
        this.props.store.reducerManager.remove(alias);
      }

      render() {
        const { store, ...rest } = this.props;
        return <Component {...rest} />;
      }
    }
  );
export default withStore;
