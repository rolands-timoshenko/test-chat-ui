import tick from '../../utils/tick';

export const ACTIONS = {
  USER_PROFILE_ADD: '[User] set profile',
  USER_PROFILE_REMOVE: '[User] unset profile',
  USER_PROFILE_PROCESSING: '[User] set processing',
};

export const doLoginAsync = (username, password, onError) => {
  return async dispatch => {
    try {
      dispatch(setProcessing(true));
      await tick(1000);
      // TODO: async api call goes here
      // TODO: save into browser session
      dispatch(setUserProfile({ username: username }));
    } catch (e) {
      onError(e);
    } finally {
      dispatch(setProcessing(false));
    }
  };
};

export const setProcessing = inProgress => {
  return { type: ACTIONS.USER_PROFILE_PROCESSING, payload: inProgress };
};

export const setUserProfile = profile => {
  return { type: ACTIONS.USER_PROFILE_ADD, payload: profile };
};

export const removeUserProfile = () => {
  return { type: ACTIONS.USER_PROFILE_REMOVE, payload: null };
};
