import { ACTIONS } from './action';

const initialState = {
  profile: null,
  processing: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ACTIONS.USER_PROFILE_ADD:
      return {
        ...state,
        profile: { ...action.payload },
      };

    case ACTIONS.USER_PROFILE_REMOVE:
      return {
        ...state,
        profile: null,
      };

    case ACTIONS.USER_PROFILE_PROCESSING:
      return {
        ...state,
        processing: action.payload,
      };

    default:
      return state;
  }
};
