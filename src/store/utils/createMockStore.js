import { createStore } from 'redux';
export default initialState => createStore(state => state, initialState);
