import React from 'react';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { default as ChatModule } from '../../modules/chat/Chat';
import CustomAppBar from '../../shared/customAppBar/CustomBar';
import { removeUserProfile } from './../../store/user/action';

const Chat = ({ onLogout }) => {
  return (
    <>
      <CustomAppBar title="Chat" onLogout={onLogout} />
      <ChatModule />
    </>
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(removeUserProfile()),
  };
};

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  )
);

export default enhance(Chat);
