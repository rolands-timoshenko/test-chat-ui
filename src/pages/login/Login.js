import Grid from '@material-ui/core/Grid';
import { doLoginAsync } from './../../store/user/action';
import { connect } from 'react-redux';
import { compose } from 'redux';
import React from 'react';
import { withStyles } from '@material-ui/styles';
import LoginStyles from './Login.styles';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Loader from '../../shared/loader/Loader';
import SimpleReactValidator from 'simple-react-validator';

class Login extends React.Component {
  state = {
    username: null,
    password: null,
    error: null,
  };

  constructor(props) {
    super(props);
    this.validator = new SimpleReactValidator();
  }

  handleChangeUsername = evt => {
    this.setState({ username: evt.target.value });
  };

  handleChangePassword = evt => {
    this.setState({ password: evt.target.value });
  };

  handleLogin = () => {
    this.props.onLogin(
      this.state.username,
      this.state.password,
      this.handleError
    );
  };

  handleError = error => {
    this.setState({ error: error });
  };

  render() {
    this.validator.message('username', this.state.username, 'required|email');
    this.validator.message(
      'password',
      this.state.password,
      'required|min:5|max:10'
    );
    const { classes, processing } = this.props;
    return (
      <Container component="main" maxWidth="xs">
        <div className={classes.paper}>
          {processing && <Loader style={{backgroundColor: 'rgba(255, 255, 255, 0.3)'}} />}
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          {this.state.error && !processing && (
            <Grid container>
              <Grid item xs>
                Something went wrong !
              </Grid>
            </Grid>
          )}
          <form className={classes.form} noValidate>
            <TextField
              error={
                this.state.username && !this.validator.fieldValid('username')
              }
              value={this.state.username ? this.state.username : ''}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={this.handleChangeUsername}
            />
            <TextField
              error={
                this.state.password && !this.validator.fieldValid('password')
              }
              value={this.state.password ? this.state.password : ''}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
              onChange={this.handleChangePassword}
            />
            <Button
              onClick={this.handleLogin}
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={processing || !this.validator.allValid()}
            >
              Sign In
            </Button>
          </form>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {
    processing: state.user.processing,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: (username, password, onError) =>
      dispatch(doLoginAsync(username, password, onError)),
  };
};

const enhance = compose(
  withStyles(LoginStyles),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const EnhancedLogin = enhance(Login);

EnhancedLogin.displayName = 'Login';

export default EnhancedLogin;
