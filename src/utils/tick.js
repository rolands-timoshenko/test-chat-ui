const tick = async ms =>
  new Promise((resolve, reject) => {
    setTimeout(resolve, ms);
  });

export default tick;
