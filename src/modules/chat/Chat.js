import { Container } from '@material-ui/core';
import React from 'react';
import { compose } from 'redux';
import withStore from '../../store/hoc/withStore';
import reducer from './store/reducer';

const Chat = () => {
  return (
    <Container maxWidth="md" style={{ padding: 20 }}>
      <div>Chat module comming soon</div>
    </Container>
  );
};

const enhance = compose(withStore('chat', reducer));

export default enhance(Chat);
