const initialState = {
  messages: null,
  processing: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    default:
      return state;
  }
};
