import { Button, withStyles } from '@material-ui/core';
import React from 'react';
import AppTextareaStyles from './AppTextarea.styles';
import debounce from 'lodash/debounce';

const AppTextarea = ({
  classes,
  onSet,
  label,
  buttonLabel,
  defaultValue,

  ...rest
}) => {
  const fieldRef = React.createRef();

  const handleSet = () => {
    onSet(fieldRef.current.value);
  };

  const debounceHandleSet = debounce(handleSet, 1000);

  return (
    <div className={classes.root}>
      <p>{label}</p>
      <textarea
        onChange={debounceHandleSet}
        ref={fieldRef}
        {...rest}
        defaultValue={defaultValue ? defaultValue : ''}
      />
      {/* {onSet && !defaultValue && (
        <Button variant="outlined" onClick={handleSet}>
          {buttonLabel}
        </Button>
      )} */}
    </div>
  );
};

export default withStyles(AppTextareaStyles)(AppTextarea);
