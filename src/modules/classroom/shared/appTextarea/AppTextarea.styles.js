const AppTextareaStyles = theme => ({
  root: {
    '& textarea': {
      border: '1px solid grey',
      minWidth: 400,
      width: '100%',
      height: 180,
    },
  },
});

export default AppTextareaStyles;
