import { TextField, Button } from '@material-ui/core';
import React from 'react';
import * as Peer from 'simple-peer';
import Board from '../board/Board';

class Classroom extends React.Component {
  defaultProps = {
    teacher: false,
  };

  peers = [];

  currentIdField = React.createRef();
  otherIdField = React.createRef();

  state = {
    message: '',
    messages: [],
  };

  componentDidMount() {}

  componentWillUnmount() {
    // TODO: destroy peer connection
    this.peers = null;
  }

  addPeer = () => {};

  handleSend = () => {
    // TODO: Check if someone connected before sending a message
    this.peer.send(this.state.message);
    this.setState({ message: '' });
  };

  handleChange = evt => {
    this.setState({ message: evt.target.value });
  };

  handleConnect = () => {
    const peerId = this.otherIdField.current.value;
    this.peer.signal(JSON.parse(peerId));
  };

  handleAddPupil = () => {
    this.peers.push(
      new Peer({ initiator: this.props.teacher, trickle: false })
    );

    const currentIndex = this.peers.length - 1;

    this.peers[currentIndex].on('error', err => console.log('error', err));

    this.peers[currentIndex].on('signal', data => {
      this.currentIdField.current.value = JSON.stringify(data);
    });

    this.peers[currentIndex].on('connect', () => {
      console.info('Connected ' + currentIndex);
    });

    this.peers[currentIndex].on('data', data => {
      var dataAsText = new TextDecoder('utf-8').decode(data);
      this.setState(prevState => ({
        messages: [...prevState.messages, dataAsText],
      }));
    });
  };

  handleJoinClassroom = () => {
    this.handleAddPupil();
  };

  render() {
    return (
      <div>
        Current ID:
        <textarea ref={this.currentIdField} />
        <br />
        Other ID:
        <textarea ref={this.otherIdField} />
        {this.props.teacher && (
          <Button variant="contained" onClick={this.handleAddPupil}>
            Add pupil
          </Button>
        )}
        <hr />
        <TextField
          label="Message"
          multiline
          rowsMax="4"
          value={this.state.message}
          onChange={this.handleChange}
          margin="normal"
          style={{ width: '100%' }}
        />
        <hr />
        <Button variant="contained" onClick={this.handleSend}>
          Send
        </Button>
        <Board>
          <ul>
            {this.state.messages.map((message, index) => (
              <li key={index}>{message}</li>
            ))}
          </ul>
        </Board>
      </div>
    );
  }
}

export default Classroom;
