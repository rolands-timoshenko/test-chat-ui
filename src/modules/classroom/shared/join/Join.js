import {
  Button,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@material-ui/core';
import React, { useState } from 'react';
import AppTextarea from '../appTextarea/AppTextarea';
import CustomDialog from '../customDialog/CustomDialog';

const Join = ({ classroom, onConnect, onMasterIdSet }) => {
  const [dialogOpen, setDialogOpen] = useState(false);

  return (
    <>
      <Button
        onClick={() => setDialogOpen(true)}
        variant={classroom && classroom.connected ? 'contained' : 'outlined'}
        color="secondary"
        disabled={!classroom}
      >
        {classroom && classroom.connected ? 'Joined' : 'Join'}
      </Button>
      <CustomDialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
        <DialogTitle>Join to the classroom</DialogTitle>
        <DialogContent>
          {classroom && classroom.slaveId && (
            <AppTextarea
              label="Paste slave id to teacher."
              readOnly
              defaultValue={classroom.slaveId}
            />
          )}
          {classroom && !classroom.slaveId && (
            <AppTextarea
              label="Paste here master id."
              onSet={signal => onMasterIdSet(signal)}
              buttonLabel="Set master id"
            />
          )}
        </DialogContent>
        <DialogActions>
          <Button
            variant="outlined"
            onClick={() => setDialogOpen(false)}
            color="secondary"
          >
            Close
          </Button>
          {classroom && classroom.masterId && (
            <Button
              variant="contained"
              onClick={onConnect}
              disabled={!!classroom.slaveId || classroom.connected}
              color="primary"
            >
              {classroom.connected ? 'Connected' : 'Connect'}
            </Button>
          )}
        </DialogActions>
      </CustomDialog>
    </>
  );
};

export default Join;
