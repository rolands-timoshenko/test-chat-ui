import Button from '@material-ui/core/Button';
import React from 'react';
import { withStyles } from '@material-ui/core';
import StudentsStyles from './Students.styles';
import Student from './student/Student';

const Students = ({
  classes,
  students,
  onConnect,
  onDisconnect,
  onAddStudent,
  onSlaveSet,
}) => {
  return (
    <div className={classes.root}>
      <div className={classes.list}>
        {students.map(student => (
          <Student
            onSlaveSet={onSlaveSet}
            onConnect={onConnect}
            onDisconnect={onDisconnect}
            key={student.id}
            student={student}
          />
        ))}
      </div>
      <Button variant="outlined" onClick={onAddStudent}>
        Add student
      </Button>
    </div>
  );
};

export default withStyles(StudentsStyles)(Students);
