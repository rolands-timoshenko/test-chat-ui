import { DialogActions, DialogContent, DialogTitle } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import React, { useState } from 'react';
import AppTextarea from '../../appTextarea/AppTextarea';
import CustomDialog from '../../customDialog/CustomDialog';
import StudentStyles from './Student.styles';

const Student = ({ classes, student, onSlaveSet, onConnect, onDisconnect }) => {
  const [open, setOpen] = useState(false);

  return (
    <div className={classes.root}>
      <Button
        disabled={!student.masterId}
        onClick={() => setOpen(true)}
        variant={student.connected ? 'contained' : 'outlined'}
        color="secondary"
      >
        {student.name}
      </Button>
      <CustomDialog open={open} onClose={() => setOpen(false)}>
        <DialogTitle>{student.name}</DialogTitle>
        <DialogContent>
          {!student.connected && (
            <>
              <AppTextarea
                label="Master ID"
                readOnly
                defaultValue={student.masterId}
              />
              <AppTextarea
                label="Slave ID"
                buttonLabel="Set slave id"
                defaultValue={student.slaveId}
                readOnly={student.slaveId}
                onSet={slaveId => onSlaveSet(student, slaveId)}
              />
            </>
          )}
          {student.messages.length > 0 && (
            <ul>
              {student.messages.map((message, index) => (
                <li key={index}>{message}</li>
              ))}
            </ul>
          )}
        </DialogContent>
        <DialogActions>
          <Button
            variant="outlined"
            onClick={() => setOpen(false)}
            color="secondary"
          >
            Close
          </Button>
          <Button
            variant="contained"
            onClick={
              student.connected
                ? () => onDisconnect(student)
                : () => onConnect(student)
            }
            disabled={!student.slaveId}
            color="primary"
          >
            {student.connected ? 'Disconnect' : 'Connect'}
          </Button>
        </DialogActions>
      </CustomDialog>
    </div>
  );
};

export default withStyles(StudentStyles)(Student);
