const StudentsStyles = theme => ({
  list: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 -5px',
  },
});

export default StudentsStyles;
