import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import React from 'react';
import BoardStyles from './Board.styles';

const Board = ({ classes, onClear, list }) => {
  return (
    <>
      <div className={classes.root}>
        <ul>
          {list.map((message, index) => (
            <li key={index}>{message}</li>
          ))}
        </ul>
      </div>
      {onClear && (
        <Button
          onClick={onClear}
          disabled={list.length < 1}
          variant="contained"
          classes={{ root: classes.rootButton }}
        >
          Clear board
        </Button>
      )}
    </>
  );
};

export default withStyles(BoardStyles)(Board);
