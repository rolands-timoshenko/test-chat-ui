const BoardStyles = theme => ({
  root: {
    border: '20px solid brown',
    backgroundColor: 'green',
    minHeight: 200,
    color: 'white',
    padding: 20,
    '& ul': {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  rootButton: {
    marginTop: 10,
  },
});

export default BoardStyles;
