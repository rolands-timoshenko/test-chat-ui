import React, { useState } from 'react';
import { TextField, Button } from '@material-ui/core';

const Message = ({ onSend }) => {
  const [message, setMessage] = useState('');

  const handleChangeMessage = evt => {
    setMessage(evt.target.value);
  };

  const handleSend = () => {
    onSend(message);
    setMessage('');
  };

  return (
    <div
      style={{ border: '1px solid lightGrey', margin: '10px 0', padding: 10 }}
    >
      <TextField
        label="Message"
        multiline
        rowsMax="4"
        value={message}
        onChange={handleChangeMessage}
        margin="normal"
        style={{ width: '100%' }}
      />
      <Button variant="contained" onClick={handleSend}>
        Send
      </Button>
    </div>
  );
};

export default Message;
