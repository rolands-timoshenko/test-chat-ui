import React from 'react';
import { Dialog, withStyles } from '@material-ui/core';
import CustomDialogStyles from './CustomDialog.styles';

const CustomDialog = ({ classes, children, ...rest }) => {
  return (
    <Dialog classes={classes} {...rest}>
      {children}
    </Dialog>
  );
};

export default withStyles(CustomDialogStyles)(CustomDialog);
