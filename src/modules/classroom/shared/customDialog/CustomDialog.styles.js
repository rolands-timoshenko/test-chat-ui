const CustomDialogStyles = theme => ({
  paper: {
    minWidth: 500,
    minHeight: 300,
  },
});

export default CustomDialogStyles;
