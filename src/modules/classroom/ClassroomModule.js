import { Switch, Route } from 'react-router-dom';
import Container from '@material-ui/core/Container';
import React from 'react';
import urls from './config/urls';
import Entry from './pages/entry/Entry';
import Teacher from './pages/teacher/Teacher';
import Student from './pages/student/Student';

const ClassroomModule = () => {
  return (
    <Container maxWidth="md" style={{ padding: 20 }}>
      <Switch>
        <Route exact path={urls.Classroom} component={Entry} />
        <Route path={urls.Teacher} component={Teacher} />
        <Route path={urls.Student} component={Student} />
      </Switch>
    </Container>
  );
};

export default ClassroomModule;
