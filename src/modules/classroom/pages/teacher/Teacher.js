import React from 'react';
import * as Peer from 'simple-peer';
import * as uuidv4 from 'uuid/v4';
import Board from '../../shared/board/Board';
import Message from '../../shared/message/Message';
import Students from '../../shared/students/Students';

class Teacher extends React.Component {
  state = {
    messages: [],
    students: [],
  };

  handleAddStudent = () => {
    const student = {
      id: uuidv4(),
      name: `Student ${this.state.students.length + 1}`,
      peer: null,
      masterId: null,
      slaveId: null,
      connected: false,
      messages: [],
      error: null,
    };

    student.peer = new Peer({
      initiator: true,
      trickle: false,
    });

    student.peer.on('error', err => {
      this.setState(prevState => ({
        students: prevState.students.map(item => {
          if (item.id === student.id) {
            return { ...item, error: err };
          }
          return item;
        }),
      }));
    });

    student.peer.on('signal', data => {
      this.setState(prevState => ({
        students: prevState.students.map(item => {
          if (item.id === student.id) {
            return { ...item, masterId: JSON.stringify(data) };
          }
          return item;
        }),
      }));
    });

    student.peer.on('connect', () => {
      this.setState(prevState => ({
        students: prevState.students.map(item => {
          if (item.id === student.id) {
            return { ...item, connected: true };
          }
          return item;
        }),
      }));
    });

    student.peer.on('data', data => {
      var dataAsText = new TextDecoder('utf-8').decode(data);
      this.setState(prevState => ({
        students: prevState.students.map(item => {
          if (item.id === student.id) {
            return { ...item, messages: [...item.messages, dataAsText] };
          }
          return item;
        }),
      }));
    });

    this.setState(prevState => ({
      students: [...prevState.students, student],
    }));
  };

  handleDisConnect = student => {
    student.peer.destroy();
    this.setState(prevState => ({
      students: prevState.students.filter(item => item.id !== student.id),
    }));
  };

  handleSetSlave = (student, slaveId) => {
    this.setState(prevState => ({
      students: prevState.students.map(item => {
        if (item.id === student.id) {
          return { ...item, slaveId: slaveId };
        }
        return item;
      }),
    }));
  };

  handleSend = message => {
    this.setState(
      prevState => ({
        messages: [...prevState.messages, message],
      }),
      () => {
        this.state.students.forEach(student => {
          if (student.connected) {
            student.peer.send(JSON.stringify(this.state.messages));
          }
        });
      }
    );
  };

  handleConnect = student => {
    student.slaveId && student.peer.signal(JSON.parse(student.slaveId));
  };

  handleClearMessages = () => {
    this.setState(
      {
        messages: [],
      },
      () => {
        this.state.students.forEach(student => {
          if (student.connected) {
            student.peer.send(JSON.stringify(this.state.messages));
          }
        });
      }
    );
  };

  render() {
    return (
      <>
        <Students
          onAddStudent={this.handleAddStudent}
          onConnect={this.handleConnect}
          onDisconnect={this.handleDisConnect}
          onSlaveSet={this.handleSetSlave}
          students={this.state.students}
        />
        <Message onSend={this.handleSend} />
        <Board onClear={this.handleClearMessages} list={this.state.messages} />
      </>
    );
  }
}

export default Teacher;
