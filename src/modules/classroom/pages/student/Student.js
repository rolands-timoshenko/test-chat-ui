import React from 'react';
import * as Peer from 'simple-peer';
import Board from '../../shared/board/Board';
import Join from '../../shared/join/Join';
import Message from '../../shared/message/Message';

class Student extends React.Component {
  state = {
    messages: [],
    classroom: null,
  };

  handleSend = message => {
    this.state.classroom &&
      this.state.classroom.connected &&
      this.state.classroom.peer.send(message);
  };

  handleConnect = () => {
    this.state.classroom.peer.signal(JSON.parse(this.state.classroom.masterId));
  };

  componentDidMount() {
    this.setClassroom();
  }

  setClassroom = () => {
    const classroom = {
      slaveId: null,
      peer: null,
      masterId: null,
      connected: false,
      error: null,
    };

    classroom.peer = new Peer({ initiator: false, trickle: false });

    classroom.peer.on('error', err => this.setState({ error: err }));

    classroom.peer.on('signal', data => {
      const slaveId = JSON.stringify(data);
      this.setState(prevState => ({
        classroom: { ...prevState.classroom, slaveId: slaveId },
      }));
    });

    classroom.peer.on('connect', () => {
      this.setState(prevState => ({
        classroom: { ...prevState.classroom, connected: true },
      }));
    });

    classroom.peer.on('data', data => {
      const dataAsJSON = new TextDecoder('utf-8').decode(data);
      const messages = JSON.parse(dataAsJSON);
      this.setState({
        messages: [...messages],
      });
    });

    classroom.peer.on('close', () => {
      this.setState(
        _ => ({
          classroom: null,
        }),
        () => this.setClassroom()
      );
    });

    this.setState({
      classroom: classroom,
    });
  };

  handleSetMaster = masterId => {
    this.setState(prevState => ({
      classroom: {
        ...prevState.classroom,
        masterId: masterId,
      },
    }));
  };

  render() {
    return (
      <>
        <Join
          classroom={this.state.classroom}
          onMasterIdSet={this.handleSetMaster}
          onConnect={this.handleConnect}
        />
        <Message onSend={this.handleSend} />
        <Board list={this.state.messages} />
      </>
    );
  }
}

export default Student;
