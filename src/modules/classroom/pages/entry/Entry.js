import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/styles';
import React from 'react';
import { compose } from 'redux';
import EntryStyles from './Entry.styles';
import urls from '../../config/urls';

const Entry = ({ classes, history }) => {
  const navigate = url => {
    history.push(url);
  };
  return (
    <div className={classes.root}>
      <p>Choose type: </p>
      <Button variant="contained" onClick={() => navigate(urls.Teacher)}>
        Im teacher
      </Button>
      <Button variant="contained" onClick={() => navigate(urls.Student)}>
        Im student
      </Button>
    </div>
  );
};

const enhance = compose(withStyles(EntryStyles));

export default enhance(Entry);
