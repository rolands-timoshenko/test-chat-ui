export default {
  Classroom: '/classroom',
  Teacher: `/classroom/teacher`,
  Student: `/classroom/student`,
};
