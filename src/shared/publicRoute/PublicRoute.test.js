import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import createMockStore from '../../store/utils/createMockStore';
import PublicRoute from './PublicRoute';

describe('PublicRoute component: ', () => {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should redirect away from public if user logged in.', () => {
    const mockStore = createMockStore({ user: { profile: {} } });
    act(() => {
      ReactDOM.render(
        <Provider store={mockStore}>
          <BrowserRouter>
            <PublicRoute path={'/'} component={() => <div id="public" />} />
          </BrowserRouter>
        </Provider>,
        container
      );
    });
    const publicContainer = container.querySelector('#public');
    expect(publicContainer).toBeFalsy();
  });

  it('should show public page if user is not logged in.', () => {
    const mockStore = createMockStore({ user: { profile: null } });
    act(() => {
      ReactDOM.render(
        <Provider store={mockStore}>
          <BrowserRouter>
            <PublicRoute path={'/'} component={() => <div id="public" />} />
          </BrowserRouter>
        </Provider>,
        container
      );
    });
    const publicContainer = container.querySelector('#public');
    expect(publicContainer).toBeDefined();
  });
});
