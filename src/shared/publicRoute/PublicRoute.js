import { compose } from 'redux';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

class PublicRoute extends Component {
  static propTypes = {
    component: PropTypes.any,
    profile: PropTypes.object,
    redirectUrl: PropTypes.string,
  };

  render() {
    const { component: Component, profile, ...rest } = this.props;
    return (
      <Route
        {...rest}
        render={props =>
          !profile ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: this.props.redirectUrl,
              }}
            />
          )
        }
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    profile: state.user.profile,
  };
};

const enhance = compose(
  connect(
    mapStateToProps,
    null
  )
);

export default enhance(PublicRoute);
