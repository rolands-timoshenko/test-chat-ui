import React from 'react';
import ReactDOM from 'react-dom';
import { act } from 'react-dom/test-utils';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import createMockStore from '../../store/utils/createMockStore';
import ProtectedRoute from './ProtectedRoute';

describe('ProtectedRoute component: ', () => {
  let container;

  beforeEach(() => {
    container = document.createElement('div');
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
    container = null;
  });

  it('should redirect away from protected if user is not logged in.', () => {
    const mockStore = createMockStore({ user: { profile: null } });
    act(() => {
      ReactDOM.render(
        <Provider store={mockStore}>
          <BrowserRouter>
            <ProtectedRoute
              path={'/'}
              component={() => <div id="protected" />}
            />
          </BrowserRouter>
        </Provider>,
        container
      );
    });
    const protectedContainer = container.querySelector('#protected');
    expect(protectedContainer).toBeFalsy();
  });

  it('should show protected page if user is logged in.', () => {
    const mockStore = createMockStore({ user: { profile: {} } });
    act(() => {
      ReactDOM.render(
        <Provider store={mockStore}>
          <BrowserRouter>
            <ProtectedRoute
              path={'/'}
              component={() => <div id="protected" />}
            />
          </BrowserRouter>
        </Provider>,
        container
      );
    });
    const protectedContainer = container.querySelector('#protected');
    expect(protectedContainer).toBeDefined();
  });
});
