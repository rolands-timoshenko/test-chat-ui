import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/styles';
import LoaderStyles from './Loader.styles';

const Loader = ({ classes, style }) => {
  return (
    <div style={style} className={classes.root}>
      <CircularProgress className={classes.progress} />
    </div>
  );
};

export default withStyles(LoaderStyles)(Loader);
